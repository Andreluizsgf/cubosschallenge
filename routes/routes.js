// import other routes
const userRoutes = require('./users');
const queueRoutes = require('./queue')

const appRouter = (app, fs) => {

    // default route
    app.get('/', (req, res) => {
        res.send('hello world, welcome to the my api');
    });

    // // other routes

    userRoutes(app, fs);
    queueRoutes(app, fs);

};

module.exports = appRouter;