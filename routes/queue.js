const userRoutes = (app, fs) => {

    const dataPath = './data/queue.json'; // Path to JSON file containing the Queue.
    const _ = require("underscore"); // Package that helps filtering json.
    const userPath = './data/users.json'; // Path to JSON file containing Users.
    
    // helper method to read JSON FILE.
    const readFile = (callback, returnJson = false, filePath = dataPath, encoding = 'utf8') => { 
        fs.readFile(filePath, encoding, (err, data) => {
            if (err) {
                throw err;
            }
            callback(returnJson ? JSON.parse(data) : data);
        });
    };

    // helper method to write on JSON FILE.
    const writeFile = (fileData, callback, filePath = dataPath, encoding = 'utf8') => {

        fs.writeFile(filePath, fileData, encoding, (err) => {
            if (err) {
                throw err;
            }

            callback();
        });
    };

    // List all users on the queue.
    app.get('/showLine', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            if (data == '{}') 
                res.status(500).send('A fila está vazia.');
            else
                res.send(JSON.parse(data));
        });
    });

    // Add a created user to the queue.
    app.post('/addToLine/', (req, res) => {

        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            const id = parseInt(req.body.id); // Gets id passed through the path route.
            const users = JSON.parse(data);  // Gets users.json.
            const filtered = _.findWhere(users, {id: id}); // Filters users.json by id.
            if (filtered) // if filtered, that means there is an user with that id on the queue.
                res.status(500).send('O usuário já está na fila.')
            else {
                fs.readFile(userPath, 'utf8', (err, data) => {
                    if (err) {
                        throw err;
                    }
                    const id = parseInt(req.body.id); // Gets id passed through the path route.
                    const users = JSON.parse(data);  // Gets users.json.
                    const filtered = _.findWhere(users, {id: id}); // Filters users.json by id.
                    
                    if(typeof JSON.stringify(filtered) === 'undefined') res.status(409).send('Não existe usuário com esse id. Tente novamente com um válido!')
                    else{
                    readFile(data => {
                            
                        const newUserPos = Object.keys(data).length + 1; // Gets a new position to the User.
                        filtered.pos = newUserPos;
                        data[newUserPos] = filtered; // Add the new user on queue.
                        writeFile(JSON.stringify(data, null, 2), () => {
                            res.sendStatus(200).send(newUserPos);
                        });
                    },
                            true);
                }
                    })

            }
            })

        
    });

    // Pops the first element from the queue.
    app.delete('/popLine', (req, res) => {

            fs.readFile(dataPath, 'utf8', (err, data) => {
                if (err) {
                    throw err;
                }
                const users = JSON.parse(data);  // Get users.json.
                const filtered = _.each(users, function(user){ // Decrement the position of each user on queue.
                    user.pos--; 
                });

                if (data == '{}') // If data is '{}' means that queue is empty.
                    res.status(500).send('A fila está vazia.');
                else {
                readFile(data => {
                    data = filtered;
                    const id = Object.keys(data)[0]; // Get the index from the first user on queue.
                    const removed = data[id];
                    delete data[id]; // Remove the first element from the Line.
                
                    writeFile(JSON.stringify(data, null, 2), () => {
                        res.status(200).send(removed);
                    });
                },
                    true);
            }
            })

        
    });

    // List user from the queue by specified email.
    app.get('/findPosition/', (req, res) => {

        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            const email = req.body.email; // Gets email passed through the path route.
            const json = data;
            const users = JSON.parse(json);
            const filtered = _.findWhere(users, {email: email});
            if(typeof filtered == 'undefined') // If filtered is undefined means that no user were found.
                res.status(500).send('Usuário inexistente na fila.');
            else
                res.status(200).send(JSON.stringify(filtered.pos));
        });
    });

    //List user from the queue by specified gender.
    app.get('/filterLine/', (req, res) => {
    
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            const gender = req.body.gênero;
            const json = data;
            const users = JSON.parse(json);
            const filtered = _.where(users, {gênero: gender}); // Gets all users with the specified gender.
            _.each(filtered, function(user) { // Deletes unuseful attributes to show.
                delete(user.id);
                delete(user.pos);
            });
            if(JSON.stringify(filtered) === '[]') 
                res.status(500).send(`Não existem usuários do gênero ${gender} na fila`)
            else
                res.send(filtered);
            
        });
    });
};

module.exports = userRoutes;