const userRoutes = (app, fs) => {

    // Variable that contains users JSON file.
    const dataPath = './data/users.json';
    const _ = require("underscore"); // Package that helps filtering json.

    // Helper method to read files.
    const readFile = (callback, returnJson = false, filePath = dataPath, encoding = 'utf8') => {
        fs.readFile(filePath, encoding, (err, data) => {
            if (err) {
                throw err;
            }

            callback(returnJson ? JSON.parse(data) : data);
        });
    };

    // Helper method to read files.
    const writeFile = (fileData, callback, filePath = dataPath, encoding = 'utf8') => {

        fs.writeFile(filePath, fileData, encoding, (err) => {
            if (err) {
                throw err;
            }

            callback();
        });
    };

    // Route to display all users in JSON file.
    app.get('/users', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }

            res.send(JSON.parse(data));
        });
    });

    // Route to insert a new user in JSON file.
    app.post('/createUser', (req, res) => {

        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }
            const email = req.body.email; // Gets id passed through the path route.
            var users = JSON.parse(data);  // Gets users.json.
            var filtered = _.findWhere(users, {email: email}); // Filters users.json by id.
            if (filtered) 
                res.status(500).send('O usuário já está cadastrado.') // if filtered, that means there is an user with that id on the queue.
            else {
                readFile(data => {
                
                    const newUserId = Object.keys(data).length + 1;
                    data[newUserId] = req.body;
                    data[newUserId].id = newUserId;
                    
                    writeFile(JSON.stringify(data, null, 2), () => {
                        res.status(200).send('Novo usuário cadastrado.');
                    });
                },
                    true);
                
            }
        })
        
    });

};

module.exports = userRoutes;